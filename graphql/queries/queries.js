const { GraphQLList, GraphQLString, GraphQLObjectType } = require('graphql');
const TransaksiType = require('../types/transaksiTypes');
const BarangType = require('../types/barangTypes');
const Transaksi = require('../../models/transaksiModel');
const { redisClient } = require('../../redis/redis');
const { db_postgre } = require('../../database/db');

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    transaksi: {
      type: new GraphQLList(TransaksiType),
      async resolve(parent, args) {
        try {
          const dataTransaksi = await Transaksi.find().exec();
          return dataTransaksi.map(transaksi => ({
            ...transaksi._doc,
            date: new Date(transaksi.date).toISOString()
          }));
        } catch (error) {
          throw new Error('Error fetching transaksi data');
        }
      }
    },
    transaksiByQRCode: {
      type: new GraphQLList(TransaksiType),
      args: {
        qr_code: { type: GraphQLString }
      },
      async resolve(parent, args) {
        const { qr_code } = args;
        const keys = await redisClient.keys(`transaksi:${qr_code}:*`);
        const transaksiList = await Promise.all(
          keys.map(async (key) => {
            const rawData = await redisClient.get(key);
            return JSON.parse(rawData);
          })
        );
        return transaksiList;
      }
    },
    historyTransaksiCustomer: {
      type: new GraphQLList(TransaksiType),
      args: {
        qrCode: { type: GraphQLString }
      },
      async resolve(parent, args) {
        const { qrCode } = args;
        try {
          let dataTransaksi;
          if (qrCode) {
            dataTransaksi = await db_postgre.any("SELECT * FROM transaksi WHERE qr_code = $1", [qrCode]);
          } else {
            dataTransaksi = await db_postgre.any("SELECT * FROM transaksi");
          }
          return dataTransaksi.map(transaksi => ({
            ...transaksi,
            date: new Date(transaksi.date).toISOString()
          }));
        } catch (error) {
          throw new Error('Error fetching history transaksi data');
        }
      }
    },
  }
});


module.exports = RootQuery;
