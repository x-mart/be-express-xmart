// graphql/types/barangTypes.js
const { GraphQLObjectType, GraphQLString, GraphQLInt } = require("graphql");

const BarangTypes = new GraphQLObjectType({
  name: "Barang",
  fields: {
    rfid: { type: GraphQLString },
    nama_barang: { type: GraphQLString },
    harga_satuan: { type: GraphQLInt },
  },
});

module.exports = BarangTypes;
