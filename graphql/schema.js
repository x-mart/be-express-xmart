// graphql/schema.js
const { GraphQLSchema } = require('graphql');
const RootQuery = require('./queries/queries');
const RootMutation = require('./mutations/mutations');

const schema = new GraphQLSchema({
  query: RootQuery,
  mutation: RootMutation
});

module.exports = schema;