//graphql/mutations/mutations.js
const {
  GraphQLString,
  GraphQLObjectType,
  GraphQLID,
  GraphQLInt,
} = require("graphql");
const TransaksiType = require("../types/transaksiTypes");
const Transaksi = require("../../models/transaksiModel");
const { redisClient } = require("../../redis/redis");

const RootMutation = new GraphQLObjectType({
  name: "RootMutationType",
  fields: {
    createTransaksi: {
      type: TransaksiType,
      args: {
        qr_code: { type: GraphQLString },
        rfid: { type: GraphQLString },
        nama_barang: { type: GraphQLString },
        harga_satuan: { type: GraphQLInt },
        jumlah: { type: GraphQLInt },
      },
      async resolve(parent, args) {
        const { qr_code, rfid, nama_barang, harga_satuan, jumlah } = args;
        const transaksi = new Transaksi({
          qr_code,
          rfid,
          nama_barang,
          harga_satuan,
          jumlah,
        });

        try {
          const savedTransaksi = await transaksi.save();
          let objectIdString = savedTransaksi._id.toString();
          let key = `transaksi:${qr_code}:${rfid}:${nama_barang}:${harga_satuan}:${jumlah}:${objectIdString}`;
          redisClient.set(key, JSON.stringify(savedTransaksi));

          return savedTransaksi;
        } catch (error) {
          console.error("Error creating transaksi:", error);
          throw new Error("Internal server error");
        }
      },
    },
    editJumlahTransaksi: {
      type: TransaksiType,
      args: {
        _id: { type: GraphQLID },
        jumlah: { type: GraphQLInt },
      },
      async resolve(parent, args) {
        const { _id, jumlah } = args;

        try {
          // Temukan transaksi berdasarkan ID
          const transaksi = await Transaksi.findById(_id);
          if (!transaksi) {
            throw new Error("Transaksi not found");
          }

          // Update jumlah transaksi
          transaksi.jumlah = jumlah;
          const updatedTransaksi = await transaksi.save();

          // Ubah ID objek menjadi string
          const objectIdString = updatedTransaksi._id.toString();

          // Hapus kunci transaksi dari Redis cache berdasarkan _id
          const keys = await redisClient.keys(`transaksi:*${objectIdString}`);

          if (keys.length > 0) {
            await Promise.all(keys.map((key) => redisClient.del(key)));
            console.log(
              `Hapus semua kunci transaksi dengan _id ${objectIdString} di Redis cache berhasil.`
            );
          } else {
            console.log(
              `Tidak ada kunci transaksi dengan _id ${objectIdString} yang ditemukan di Redis cache.`
            );
          }

          // Buat keys yang baru tetapi atribut sebelumnya sama, kecuali jumlah dan _id(objectIdString)
          const key = `transaksi:${transaksi.qr_code}:${transaksi.rfid}:${transaksi.nama_barang}:${transaksi.harga_satuan}:${jumlah}:${objectIdString}`;

          // Simpan nilai jumlah yang diperbarui ke Redis cache
          await redisClient.set(key, JSON.stringify(updatedTransaksi));
          console.log(
            `Nilai jumlah transaksi dengan ID ${objectIdString} di Redis cache berhasil diubah menjadi ${jumlah}`
          );

          // Mengembalikan objek yang sesuai dengan TransaksiType
          return updatedTransaksi;
        } catch (error) {
          console.error("Error updating jumlah transaksi:", error);
          throw new Error("Internal server error");
        }
      },
    },
  },
});

module.exports = RootMutation;
