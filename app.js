const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const cors = require("cors");
const schema = require("./graphql/schema");
const { connectDB, db_postgre } = require("./database/db");
const { transferData } = require("./transferTransaksi");
const Transaksi = require("./models/transaksiModel");
const { redisClient } = require("./redis/redis");

const app = express();
app.use(express.json());

// Koneksi ke MongoDB
connectDB();

// Koneksi ke Postgre
db_postgre.connect();

// Gunakan cors middleware
app.use(cors());

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    graphiql: true,
  })
);

// Endpoint Transfer data transaksi dari mongodb ke postgre dan menghapus transaksi redis dan mongodb
app.get("/transfer-data/:qrCode", async (req, res) => {
  try {
    const { qrCode } = req.params;
    await transferData(qrCode);
    res.status(200).json({ message: "Data transfer successful" });
  } catch (error) {
    console.error("Error transferring data:", error);
    res.status(500).json({ message: "Error transferring data" });
  }
});

// Endpoint untuk menghapus transaksi
app.delete("/delete-cart/:_id", async (req, res) => {
  const _id = req.params._id;
  const objectIdString = req.params._id;

  try {
    // Hapus data dari MongoDB
    const result = await Transaksi.findByIdAndDelete(_id);
    if (!result) {
      return res.status(404).json({ message: "Transaksi not found" });
    }

    // Hapus data dari Redis
    const keys = await redisClient.keys(`transaksi:*${objectIdString}*`);

    if (keys.length > 0) {
      await Promise.all(keys.map((key) => redisClient.del(key)));
      console.log(
        `Deleted all keys related to transaksi with _id ${_id} from Redis cache.`
      );
    } else {
      console.log(
        `No keys related to transaksi with _id ${_id} found in Redis cache.`
      );
    }
    res
      .status(200)
      .json({ message: "Transaksi deleted successfully", data: result });
  } catch (error) {
    console.error("Error deleting transaksi:", error);
    res.status(500).json({ message: "Error deleting transaksi" });
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server GraphQL running at http://localhost:${PORT}/graphql`);
});
