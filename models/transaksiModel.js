// models/transaksiModel.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const transaksiSchema = new Schema({
  qr_code: String,
  rfid: String,
  nama_barang: String,
  harga_satuan: Number,
  jumlah: Number,
  date: {
    type: Date,
    default: Date.now
  }
}, { collection: 'transaksi' });

const Transaksi = mongoose.model('Transaksi', transaksiSchema);

module.exports = Transaksi;
