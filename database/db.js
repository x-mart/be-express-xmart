//database/db.js
require('dotenv').config();
const mongoose = require('mongoose');
const pgp = require('pg-promise')();

// Koneksi MongoDB
const connectDB = async () => {
  try {
    await mongoose.connect('mongodb://localhost:27017/db_xmart_mongo', {
      useNewUrlParser: true
    });
    console.log('MongoDB connected');
  } catch (error) {
    console.error('MongoDB connection error:', error);
    process.exit(1);
  }
};

// Koneksi PostgreSQL
const db_postgre = pgp({
  user: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  host: process.env.PG_HOST,
  port: process.env.PG_PORT,
  database: process.env.PG_DATABASE,
});

db_postgre
  .connect()
  .then(() => {
    console.log("Database PostgreSQL connected");
  })
  .catch((err) => {
    console.log("Error connecting to PostgreSQL:", err);
  });

module.exports = { connectDB, db_postgre };
