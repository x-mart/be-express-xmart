const Transaksi = require("./models/transaksiModel");
const { redisClient } = require("./redis/redis");
const { db_postgre } = require("./database/db");
require("dotenv").config();

async function transferData(qrCode) {
  try {
    // Mengambil data transaksi berdasarkan QR code dari MongoDB
    const dataTransaksi = await Transaksi.find({ qr_code: qrCode });

    // Memproses data dan simpan ke PostgreSQL
    await Promise.all(
      dataTransaksi.map(async (data) => {
        const newData = {
          transaksi_id: data._id.toString(),
          qr_code: data.qr_code,
          rfid: data.rfid,
          nama_barang: data.nama_barang,
          harga_satuan: data.harga_satuan,
          jumlah: data.jumlah,
          date: data.date.toISOString(),
        };

        const existingData = await db_postgre.any(
          "SELECT * FROM transaksi WHERE transaksi_id = $1",
          [newData.transaksi_id]
        );

        if (existingData.length === 0) {
          const query =
            "INSERT INTO transaksi (transaksi_id, qr_code, rfid, nama_barang, harga_satuan, jumlah, date) VALUES ($1, $2, $3, $4, $5, $6, $7)";
          await db_postgre.none(query, [
            newData.transaksi_id,
            newData.qr_code,
            newData.rfid,
            newData.nama_barang,
            newData.harga_satuan,
            newData.jumlah,
            newData.date,
          ]);
          console.log(
            `Data dengan transaksi_id ${newData.transaksi_id} berhasil Transfer.`
          );
        } else {
          console.log(
            `Data dengan transaksi_id ${newData.transaksi_id} sudah ada, melewati proses INSERT.`
          );
        }
      })
    );

    // Hapus data dalam koleksi Transaksi MongoDB berdasarkan qr_code
    await Transaksi.deleteMany({ qr_code: qrCode });
    console.log(
      `Hapus semua data collection transaksi dengan QR code ${qrCode} di MongoDB berhasil.`
    );

    // Hapus kunci transaksi dari Redis cache berdasarkan qr_code
    const keys = await redisClient.keys(`transaksi:${qrCode}:*`);

    if (keys.length > 0) {
      await Promise.all(keys.map((key) => redisClient.del(key)));
      console.log(
        `Hapus semua kunci transaksi dengan QR code ${qrCode} di Redis cache berhasil.`
      );
    } else {
      console.log(
        `Tidak ada kunci transaksi dengan QR code ${qrCode} yang ditemukan di Redis cache.`
      );
    }

    console.log("Transfer data selesai.");
  } catch (error) {
    console.error("Gagal mentransfer data:", error);
  }
}

module.exports = { transferData };
