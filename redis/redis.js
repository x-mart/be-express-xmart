//redis/redis.js
const redis = require('redis');
require('dotenv').config();

const redisClient = redis.createClient({
    host: process.env.REDIS_HOST || "127.0.0.1",
    port: process.env.REDIS_PORT || 6379,
    db: process.env.REDIS_DB || 0
});

redisClient.connect();

redisClient.on('error', (err) => { // Menggunakan event 'error' untuk menangkap kesalahan
  console.error('Redis error:', err);
});

redisClient.on('connect', () => { // Menggunakan event 'connect' untuk mengetahui kapan koneksi terhubung
  console.log('Redis client connected');
});

module.exports = {
  redisClient,
};
